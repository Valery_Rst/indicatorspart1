package net.indiphicators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchForIdentificators {
        public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\input.txt")); 
        FileWriter writer = new FileWriter("src\\output.txt")) {
            String string = "";
            while ((string = bufferedReader.readLine()) != null) {
                final String regex = "\\b[_A-Za-z]\\w*\\b";
                Pattern pattern = Pattern.compile(regex);
                Matcher matcher = pattern.matcher(string);
                while (matcher.find()) {
                    writer.write(matcher.group(0) + "\n");
                }
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
